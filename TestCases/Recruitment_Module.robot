*** Settings ***
Suite Setup
Test Teardown     Close Browser
Resource          ../Global/Super.robot

*** Test Cases ***
TC_160 Varify User is able to Navigate the Recruitment Module
    [Setup]    ReadTestdataFromExcel    TC_160    Recruitment
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    OpenRecruitmentModule

TC_161 Verify user is able to navigate candidates page when user clicked on Recuitment module
    [Setup]    ReadTestdataFromExcel    TC_161    Recruitment
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    OpenRecruitmentModule
    Recruitement

TC_162 verify user able to select job title from dropdown list
    [Setup]    ReadTestdataFromExcel    TC_162    Recruitment
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    OpenRecruitmentModule
    List Elements

TC_163 Verify user able to select vacancy from dropdown list
    [Setup]    ReadTestdataFromExcel    TC_163    Recruitment
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    OpenRecruitmentModule
    List Vacancy

TC_164 Verify user is able to select Hiring Manager from dropdown list
    [Setup]    ReadTestdataFromExcel    TC_164    Recruitment
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    OpenRecruitmentModule
    List Hiring manager

TC_165 Verify is able to select status from dropdown list
    [Setup]    ReadTestdataFromExcel    TC_165    Recruitment
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    OpenRecruitmentModule
    List Status

TC_166 Verify user is able to enter a particular candidate name
    [Setup]    ReadTestdataFromExcel    TC_166    Recruitment
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    OpenRecruitmentModule
    Candidate name    ${testdata}[CandidateName]

TC_167 Verify user is able to search for candidate using sepecific keywords
    [Setup]    ReadTestdataFromExcel    TC_167    Recruitment
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    OpenRecruitmentModule
    Input Text Keyword    ${testdata}[Keyword]

TC_168 verify user is able to search for candidates who have applied for specific period of time
    [Setup]    ReadTestdataFromExcel    TC_168    Recruitment
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    OpenRecruitmentModule
    From Date To Date    ${testdata}[FromDate]    ${testdata}[ToDate]

TC_169 Verify use is able to search for candidates by selecting method of Application from dropdown list
    [Setup]    ReadTestdataFromExcel    TC_169    Recruitment
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    OpenRecruitmentModule
    Methods dropdown list

TC_170 Verfiy user is able to search for all required fields
    [Setup]    ReadTestdataFromExcel    TC_170    Recruitment
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    OpenRecruitmentModule
    Patricular Vacancy

TC_171 verify user is able to reset all fields when clicked on reset button
    [Setup]    ReadTestdataFromExcel    TC_171    Recruitment
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    OpenRecruitmentModule
    Reset Data    ${testdata}[CandidateName]    ${testdata}[Keyword]

TC_172 verify user is able to ADD all fields when clicked on ADD button
    [Setup]    ReadTestdataFromExcel    TC_172    Recruitment
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    OpenRecruitmentModule
    Click ADD button

TC_173 Verify user is able to add valid data when click on Add button.
    [Setup]    ReadTestdataFromExcel    TC_173    Recruitment
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    OpenRecruitmentModule
    ADD Valid Data    ${testdata}[FirstName]    ${testdata}[MiddleName]    ${testdata}[LastName]    ${testdata}[MailID]    ${testdata}[MobileNumber]
    Add.Choosefile    ${EXECDIR}\\ImportFiles\\Anki_reddy.docx
    AddDataTextFields    ${testdata}[AddKeyword]    ${testdata}[AddComment]

TC_174 Verify user is able to add invalid data when click on Add button
    [Setup]    ReadTestdataFromExcel    TC_174    Recruitment
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    OpenRecruitmentModule
    Add Invalid Data    ${testdata}[FirstName]    ${testdata}[MiddleName]    ${testdata}[LastName]    ${testdata}[MailID]    ${testdata}[MobileNumber]
    Add.Choosefile    ${EXECDIR}\\ImportFiles\\Anki_reddy.docx
    AddDataTextFields    ${testdata}[AddKeyword]    ${testdata}[AddComment]
    ErrorMassageRequired

TC_175 Varify user is able to add button by giving the Null data.
    [Setup]    ReadTestdataFromExcel    TC_174    Recruitment
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    OpenRecruitmentModule
    Null Data
    Comment    Null data is given and click on add button

TC_177 Verify user is able to Edit button is enabled
    [Setup]    ReadTestdataFromExcel    TC_177    Recruitment
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    OpenRecruitmentModule
    ADD Valid Data    ${testdata}[FirstName]    ${testdata}[MiddleName]    ${testdata}[LastName]    ${testdata}[MailID]    ${testdata}[MobileNumber]
    Add.Choosefile    ${EXECDIR}\\ImportFiles\\Anki_reddy.docx
    AddDataTextFields    ${testdata}[AddKeyword]    ${testdata}[AddComment]
    Edit Data

TC_178 Verify user is able to cancel edit data when clicked on Cancel button
    [Setup]    ReadTestdataFromExcel    TC_178    Recruitment
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    OpenRecruitmentModule
    ADD Valid Data    ${testdata}[FirstName]    ${testdata}[MiddleName]    ${testdata}[LastName]    ${testdata}[MailID]    ${testdata}[MobileNumber]
    Add.Choosefile    ${EXECDIR}\\ImportFiles\\Anki_reddy.docx
    AddDataTextFields    ${testdata}[AddKeyword]    ${testdata}[AddComment]
    Edit Data
    Cancel Data

TC_179 Verify user is able to back button is enabled
    [Setup]    ReadTestdataFromExcel    TC_179    Recruitment
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    OpenRecruitmentModule
    ADD Valid Data    ${testdata}[FirstName]    ${testdata}[MiddleName]    ${testdata}[LastName]    ${testdata}[MailID]    ${testdata}[MobileNumber]
    Add.Choosefile    ${EXECDIR}\\ImportFiles\\Anki_reddy.docx
    AddDataTextFields    ${testdata}[AddKeyword]    ${testdata}[AddComment]
    Edit Data
    Sleep    2
    Click Button    ${Recruitment.ADD.Savebutton}
    back to navigate

TC_186 Verify user is able to search for all required field's
    [Setup]    ReadTestdataFromExcel    TC_186    Recruitment
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    OpenRecruitmentModule
    Search Vacancy
