*** Settings ***
Resource          ../Global/Super.robot
Resource          ../Keyword/TimeKeyword.robot

*** Test Cases ***
Tc_221 User able to see attendance summary
    [Setup]    ReadTestdataFromExcel    TC_222    Time
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    OpenTimeModule
    ClickOnReportsModule
    ClickOnAttendanceSummary

TC_222Verify user able to addcustomer with combination of numbers and specialcharecters
    [Setup]    ReadTestdataFromExcel    TC_222    Time
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Time
    ClickOnProjectInfoDropdown
    ClickOnCustomers
    Add Customers    ${testdata}[Customername]

TC_223User able add the customer
    [Setup]    ReadTestdataFromExcel    TC_223    Time
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Time
    ClickOnProjectInfoDropdown
    ClickOnCustomers
    Add Customers    ${testdata}[Customername]

TC-224 Verify by leaving the "customer name blank" and try to add
    [Setup]    ReadTestdataFromExcel    TC_223    Time
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Time
    ClickOnProjectInfoDropdown
    ClickOnCustomers
    Add customer with customer \ name is \ blank

TC_225 Verify user is able to "delete customer"
    [Setup]    ReadTestdataFromExcel    TC_222    Time
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Time
    ClickOnProjectInfoDropdown
    ClickOnCustomers
    Comment    Add Customers    Srinu
    Delete Customer

TC_226 Verify user is able to search the project.
    [Setup]    ReadTestdataFromExcel    TC_222    Time
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Time
    ClickOnProjectInfoDropdown
    Searchfor theproject

TC_227 Verify by leaving all the fields blank and try to search project
    [Setup]    ReadTestdataFromExcel    TC_222    Time
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Time
    ClickOnProjectInfoDropdown
    Search project with blank details

TC_437 Verify user is able to access Employee List
    [Setup]    ReadTestdataFromExcel    TC_222    Time
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    PIM
    Open Employee List

TC_438 Verify user is able to search Employee List
    [Setup]    ReadTestdataFromExcel    TC_222    Time
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    PIM
    Open Employee List
    Search Employee List

TC_443Verify user is able to access Add Employee
    [Setup]    ReadTestdataFromExcel    TC_222    Time
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    PIM
    Open Employee List
    Add employee

TC_446 Verify user able to acess Reports
    [Setup]    ReadTestdataFromExcel    TC_222    Time
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    PIM
    Acess Reports
