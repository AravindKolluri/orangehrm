*** Settings ***
Resource          ../Global/Super.robot

*** Test Cases ***
TC_63 Verify user is able to open OrangeHRM apllication
    [Setup]
    LaunchTheApplication    ${URL}    ${BROWSERNAME}

TC_64 Verify user is able to login with valid data
    [Setup]    ReadTestdataFromExcel    TC_064    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]

TC_65Verify user login by entering invalid user name and password
    [Setup]    ReadTestdataFromExcel    TC_065    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    Wait Until Element Is Visible    ${Elements.loginpage.invalid}    ${MEDIUMWAITS}    Homepage is not visible after waiting ${MEDIUMWAITS}

TC_66Verify user login without entering any details
    [Setup]    ReadTestdataFromExcel    TC_066    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    {testdata}[Username]    {testdata}[Password]
    Comment    Wait Until Element Is Visible    ${Elements.loginpage.cann't be empty }    ${LONGWAITS}    Homepage is not visible after waiting ${LONGWAITS}

TC_67 Verify user is able to open leave module
    [Setup]    ReadTestdataFromExcel    TC_067    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Leave

TC_140 Verify wheather the search button is working
    [Setup]    ReadTestdataFromExcel    TC_138    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Leave
    open leavelists
    calender
    check box one Leavelist
    click on search

TC_141 Verify user able to navigate the search button by entering correct data in the input boxes from all checkboxes
    [Setup]    ReadTestdataFromExcel    TC_138    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Leave
    open leavelists
    calender
    checkboxAll    ${checkbox.tab}
    Comment    check box one Leavelist
    click on search
    Comment    select actions

TC_142 Verify wheather the user able to navigate select action module
    [Setup]    ReadTestdataFromExcel    TC_141    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Leave
    open leavelists
    calender
    checkboxAll    ${Elements.checkboxAll}
    Employee    ${testdata}[Employee]
    click on search
    select actions
    select date

TC_143 Verify wheather the save button is working
    [Setup]    ReadTestdataFromExcel    TC_141    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Leave
    open leavelists
    calender
    checkboxAll    ${Elements.checkboxAll}
    Employee    ${testdata}[Employee]
    click on search
    select actions
    click on save

TC_144 Verify wheather the back button is working
    [Setup]    ReadTestdataFromExcel    TC_141    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Leave
    open leavelists
    calender
    checkboxAll    ${Elements.checkboxAll}
    Employee    ${testdata}[Employee]
    click on search
    select actions
    select date
    click on back    ${button.back}

TC_145 Veify the reset button by entering data clicking on it
    [Setup]    ReadTestdataFromExcel    TC_141    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Leave
    open leavelists
    calender
    Comment    checkboxAll
    Comment    Employee    ${testdata}[Employee]
    click on search
    Comment    select actions
    select date
    click on back    ${button.back}
    click on Reset

TC_68 Verify user is able to view submodules of entitlement
    [Setup]    ReadTestdataFromExcel    TC_068    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Leave
    NavigateToLeaveSubModule    Entitlements

TC_69 Verify user is able to open add entitlement page
    [Setup]    ReadTestdataFromExcel    TC_069    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Leave
    NavigateToLeaveSubModule    Entitlements
    OpenAddEntitlement

TC_70 Verify the save button by entering the correct data in input boxes
    [Setup]    ReadTestdataFromExcel    TC_070    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Leave
    NavigateToLeaveSubModule    Entitlements
    OpenAddEntitlement
    Enter Details in Add Leave Entitlement    ${testdata}[Name]    ${testdata}[Entitlement]

TC_74 Verify wheather cancel is working
    [Setup]    ReadTestdataFromExcel    TC_074    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Leave
    NavigateToLeaveSubModule    Entitlements
    OpenAddEntitlement
    AddLeave Cancel button

TC_72 Verify wheather confirm button working or not
    [Setup]    ReadTestdataFromExcel    TC_072    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Leave
    NavigateToLeaveSubModule    Entitlements
    OpenAddEntitlement
    Enter Details in Add Leave Entitlement    ${testdata}[Name]    ${testdata}[Entitlement]
    Updating Entitlement

TC_73 Verify wheather the save button is working with invalid data in required fields
    [Setup]    ReadTestdataFromExcel    TC_075    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Leave
    NavigateToLeaveSubModule    Entitlements
    OpenAddEntitlement
    Enter Details in Add Leave Entitlement    ${testdata}[Name]    ${testdata}[Entitlement]
    Comment    SeleniumLibrary.Wait Until Element Is Visible    ${Textfiled.leave.addleave.required}    ${LONGWAITS}

TC_75 Verify wheather entitlement is added
    [Setup]    ReadTestdataFromExcel    TC_075    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Leave
    NavigateToLeaveSubModule    Entitlements
    OpenAddEntitlement
    Enter Details in Add Leave Entitlement    ${testdata}[Name]    ${testdata}[Entitlement]
    Popup Update entitlement
    sleep     2
    Enter Details in leave Entitlement    ${testdata}[Name]

TC_76 Verifying the search button entering employee name
    [Setup]    ReadTestdataFromExcel    TC_076    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Leave
    NavigateToLeaveSubModule    Entitlements
    OpenAddEntitlement
    Enter Details in Add Leave Entitlement    ${testdata}[Name]    ${testdata}[Entitlement]
    Popup Update entitlement
    sleep    2
    Enter Details in leave Entitlement    ${testdata}[Name]

TC_87 Verify wheather the user is able to navigate the my entitlements module.
    [Setup]    ReadTestdataFromExcel    TC_086    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Leave
    Navigatetoleavesubmodule    Entitlements
    openMyEntitlement    ${homepage.entitlement.myentitlement}

TC_88 Verify wheather the search button is working.
    [Setup]    ReadTestdataFromExcel    TC_087    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Leave
    NavigateToLeaveSubModule    Entitlements
    openMyEntitlement    ${homepage.entitlement.myentitlement}
    clickonsearch    ${button.leave.leave Entitlement.search}

TC_89 Verify the save button without entering the any data in the input boxes and drop downs.
    [Setup]    ReadTestdataFromExcel    TC_087    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Leave
    NavigateToLeaveSubModule    Entitlements
    openMyEntitlement    ${homepage.entitlement.myentitlement}
    clickonsearch    ${button.leave.leave Entitlement.search}
    clickonadd
    clickonsave

TC_106 Verify wheather the apply button is working.
    [Setup]    ReadTestdataFromExcel    TC_087    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Leave
    clickonapply

TC_110 Verify wheather user is able to view the onfigure module drop down list items.
    [Setup]    ReadTestdataFromExcel    TC_087    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Leave
    clickonconfigure

TC_111 verify is user able to open the leave period module by clicking.
    [Setup]    ReadTestdataFromExcel    TC_087    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Leave
    clickonconfigure
    clickonleaveperiod

TC_120 Verify the save button without invalid data in the name input text field.
    [Setup]    ReadTestdataFromExcel    TC_120    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Leave
    Navigate to Configure
    Enter Add Button
    Enter Save Button

TC_121 Verify the cancel button by entering all the valid data in the name input text field.
    [Setup]    ReadTestdataFromExcel    TC_121    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Leave
    Navigate to Configure
    Enter Add Button
    Enter valid details in Name field    ${testdata}[Name]
    Enter Cancel button

TC_122 Verify the cancel button by enetring the invalid data in the name input text field.
    [Setup]    ReadTestdataFromExcel    TC_120    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Leave
    Navigate to Configure
    Enter Add Button
    Enter Cancel Button

TC_125 Verify is user able to open the work week module.
    [Setup]    ReadTestdataFromExcel    TC_120    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Leave
    Navigate to Configure List

TC_126 Verify the Functionality of Edit button.
    [Setup]    ReadTestdataFromExcel    TC_120    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Leave
    Navigate to Configure List
    Enter Edit Button

TC_130 Verify the save button without entering any data in all the input text fields
    [Setup]    ReadTestdataFromExcel    TC_120    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Leave
    Navigate to Configure List
    Enter Edit Button
    Save Work Week Data

TC_131 Verify wheather user is able to navigate the holidays module.
    [Setup]    ReadTestdataFromExcel    TC_120    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Leave
    Click holiday dropdown

TC_132 Verify the functionality of search button by giving previous year dates in From and To input boxes.
    [Setup]    ReadTestdataFromExcel    TC_132    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Leave
    Click holiday dropdown
    Enter From and To dates and Search    ${testdata}[fromdate]    ${testdata}[todate]

TC_133 Verify the functionality of search button by giving future year dates in From and To input boxes.
    [Setup]    ReadTestdataFromExcel    TC_133    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Leave
    Click holiday dropdown
    Enter From and To dates of Future year and Search    ${testdata}[fromdate]    ${testdata}[todate]

TC_134 Verify the font size of the data.
    [Setup]    ReadTestdataFromExcel    TC_134    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Leave
    Click holiday dropdown
    Enter From and To dates of Future year and Search    ${testdata}[fromdate]    ${testdata}[todate]

TC_135 Verify wheather hide button is working.
    [Setup]    ReadTestdataFromExcel    TC_120    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Leave
    Click holiday dropdown
    Enter Hide Button

TC_136 Verify wheather add button is working.
    [Setup]    ReadTestdataFromExcel    TC_120    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Leave
    Click holiday dropdown
    Enter Add Holiday Button

TC_111 verify user is able to open the leave period module by clicking.
    [Setup]    ReadTestdataFromExcel    TC_120    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Leave
    Navigate to Configure Leave Period Module

TC_113 Verify the functionality of edit button by clicking on it
    [Setup]    ReadTestdataFromExcel    TC_120    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Leave
    Navigate to Configure Leave Period Module
    Enter Leave Period Edit Button

TC_117 Verify user able to open the leave type module.
    [Setup]    ReadTestdataFromExcel    TC_120    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Leave
    Navigate to Configure Leave Type Module

TC_123 Verify the delete button by selecting particular leave type check box.
    [Setup]    ReadTestdataFromExcel    TC_120    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Leave
    Navigate to Configure
    Select a Particular Checkbox
    Enter Delete Button
    Enter Ok Button

TC_124 Verify the delete button by selecting multiple leave type check boxes at a time.
    [Setup]    ReadTestdataFromExcel    TC_120    Leave
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    NavigateMenu    Leave
    Navigate to Configure
    Select a Multiple Checkbox
    Enter Delete Button
    Enter Ok Button
