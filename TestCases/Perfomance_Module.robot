*** Settings ***
Suite Setup
Suite Teardown
Test Teardown     Close Browser
Resource          ../Global/Super.robot

*** Test Cases ***
TC_05_Verify if user is able to open the performance module
    [Setup]    ReadTestdataFromExcel    TC    Performance
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    Open Performance Module

TC_06_Verify configure button with the dropdown is enabled.
    [Setup]    ReadTestdataFromExcel    TC    Performance
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    Open Configure

TC_07_Verify the KPI button is enabled
    [Setup]    ReadTestdataFromExcel    TC    Performance
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    Open Performance Module
    Open Configure
    Open KPI

TC_08_Verify the job title dropdown.
    [Setup]    ReadTestdataFromExcel    TC    Performance
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    Open Performance Module
    Open Configure
    Open KPI
    Wait Until Element Is Visible    ${KPI.Jobtitle}    ${MEDIUMWAITS}
    Comment    Once element is visible,Click on Job title
    SeleniumLibrary.Click Element    ${KPI.Jobtitle}

TC_09_Verify the search button.
    [Setup]    ReadTestdataFromExcel    TC    Performance
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    Open Performance Module
    Open Configure
    Open KPI
    Wait Until Element Is Visible    ${KPI.Jobtitle}    ${MEDIUMWAITS}
    Comment    Once element is visible,Click on Job title
    SeleniumLibrary.Click Element    ${KPI.Jobtitle}
    Comment    select any job title from the dropdown
    Wait Until Element Is Visible    ${Jobtitle.dropdown}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${Jobtitle.dropdown}
    Comment    after selecting element from dropdown click on search
    Wait Until Element Is Visible    ${Jobtitle.Search}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${Jobtitle.Search}

TC_10_Verify the Add button.
    [Setup]    ReadTestdataFromExcel    TC    Performance
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    Open Performance Module
    Open Configure
    Open KPI
    Comment    Click on add button after clicking KPI
    Wait Until Element Is Visible    ${KPI.Add}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${KPI.Add}

TC_12_Verify the add button by giving the Null data.
    [Setup]    ReadTestdataFromExcel    TC    Performance
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    Open Performance Module
    Open Configure
    Open KPI
    Comment    Click on add button after clicking KPI
    Wait Until Element Is Visible    ${KPI.Add}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${KPI.Add}
    Comment    Click on Save button without giving required data
    Wait Until Element Is Visible    ${KPI.Save}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${KPI.Save}

TC_13_Verify the save button is enabled.
    [Setup]    ReadTestdataFromExcel    TC_13    Performance
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    Open Performance Module
    Open Configure
    Open KPI
    Comment    Click on add button after clicking KPI
    Wait Until Element Is Visible    ${KPI.Add}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${KPI.Add}
    Add Details in KPI    ${testdata}[KPI]    ${testdata}[Minrating]    ${testdata}[Maxrating]
    Wait Until Element Is Visible    ${KPI.Save}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${KPI.Save}

TC_14_Verify the cancel button is enabled.
    [Setup]    ReadTestdataFromExcel    TC_13    Performance
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    Open Performance Module
    Open Configure
    Open KPI
    Comment    Click on add button after clicking KPI
    Wait Until Element Is Visible    ${KPI.Add}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${KPI.Add}
    Add Details in KPI    ${testdata}[KPI]    ${testdata}[Minrating]    ${testdata}[Maxrating]
    Wait Until Element Is Visible    ${KPI.Cancel}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${KPI.Cancel}

TC_16_Verify the trackers button is enabled.
    [Setup]    ReadTestdataFromExcel    TC    Performance
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    Open Performance Module
    Open Configure
    Open Tracker

TC_15_Verify the delete button is enabled.
    [Setup]    ReadTestdataFromExcel    TC    Performance
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    Open Performance Module
    Open Configure
    Open KPI
    Comment    Click on a checkbox after clicking KPI
    Wait Until Element Is Visible    ${Delete.checkbox}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${Delete.checkbox}
    Comment    Click on a delete button after selecting a checkbox
    Wait Until Element Is Visible    ${KPI.delete}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${KPI.delete}
    SeleniumLibrary.Click Element    ${KPI.delete_DB}

TC_17_Verify the add button in trackers is enabled.
    [Setup]    ReadTestdataFromExcel    TC    Performance
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    Open Performance Module
    Open Configure
    Open Tracker
    Comment    Click on add button after clicking Trackers
    Wait Until Element Is Visible    ${Trackers.Add}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${Trackers.Add}

TC_19_Verify the add button of tracker by giving the Null data.
    [Setup]    ReadTestdataFromExcel    TC    Performance
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    Open Performance Module
    Open Configure
    Open Tracker
    Wait Until Element Is Visible    ${Trackers.Add}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${Trackers.Add}
    Comment    Click save without adding any details
    Wait Until Element Is Visible    ${Trackers.Add_save}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${Trackers.Add_save}

TC_22_Verify the delete button in trackers.
    [Setup]    ReadTestdataFromExcel    TC    Performance
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    Open Performance Module
    Open Configure
    Open Tracker
    SeleniumLibrary.Click Element    ${Tracker.deleteCB}
    Comment    Click delete after selecting the checkbox
    SeleniumLibrary.Click Element    ${Tracker.delete}
    SeleniumLibrary.Click Element    ${KPI.delete_DB}

TC_23_Verify the manage reviews button with the dropdown is enabled
    [Setup]    ReadTestdataFromExcel    TC    Performance
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    Open Performance Module
    Comment    open manage review from dropdown
    Open Manage Reviews
