*** Settings ***
Resource          ../Global/Super.robot

*** Test Cases ***
TC412_Verify if user is able to access PIM Module
    [Setup]    ReadTestdataFromExcel    TC_002    Orangehrm
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    OpenPIM

TC413_Verify user is able to access Configuration Module
    [Setup]    ReadTestdataFromExcel    TC_002    Orangehrm
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    OpenPIM
    ConfigurationPIM

TC414_Verify User is able to access Optional Field
    [Setup]    ReadTestdataFromExcel    TC_002    Orangehrm
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    OpenPIM
    ConfigurationPIM
    OptionalFieldsPIM

TC415_Verify user is able to Edit data in Optional Field section
    [Setup]    ReadTestdataFromExcel    TC_002    Orangehrm
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    OpenPIM
    ConfigurationPIM
    OptionalFieldsPIM
    EditOptionalPIM

TC416_Verify user is able to Save data in Optional Field Section
    [Setup]    ReadTestdataFromExcel    TC_002    Orangehrm
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    OpenPIM
    ConfigurationPIM
    OptionalFieldsPIM
    EditOptionalPIM
    SaveOptionalPIM

TC417_Verify user is able to access Custom Field section
    [Setup]    ReadTestdataFromExcel    TC_002    Orangehrm
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    OpenPIM
    ConfigurationPIM
    CustomFieldsPIM

TC418_Verify user is able to Click on 'Add' in Custom Field
    [Setup]    ReadTestdataFromExcel    TC_002    Orangehrm
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    OpenPIM
    ConfigurationPIM
    CustomFieldsPIM
    AddCustomPIM

TC419_Verify user is able to add data in Custom Field
    [Setup]    ReadTestdataFromExcel    TC_002    Orangehrm
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    OpenPIM
    ConfigurationPIM
    CustomFieldsPIM
    AddCustomPIM
    AddDataCustomPIM

TC420_Verify user is able to save only Field name is custom field
    [Setup]    ReadTestdataFromExcel    TC_002    Orangehrm
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    OpenPIM
    ConfigurationPIM
    CustomFieldsPIM
    AddCustomPIM
    AddOnlyTextCustomPIM

TC421_Verify user is able to save only Screen credentials in custom field
    [Setup]    ReadTestdataFromExcel    TC_002    Orangehrm
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    OpenPIM
    ConfigurationPIM
    CustomFieldsPIM
    AddCustomPIM
    AddOnlyScreenCustomPIM

TC422_Verify user is able to save only Type credentials in custom field
    [Setup]    ReadTestdataFromExcel    TC_002    Orangehrm
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    OpenPIM
    ConfigurationPIM
    CustomFieldsPIM
    AddCustomPIM
    AddOnlyTypeCustomPIM

TC423_Verify user is able to cancel \ in Custom Field
    [Setup]    ReadTestdataFromExcel    TC_002    Orangehrm
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    OpenPIM
    ConfigurationPIM
    CustomFieldsPIM
    AddCustomPIM
    Click Element    ${CustomsCancelButton}

TC424_Verify user is able to access Data Import in Configuration Module
    [Setup]    ReadTestdataFromExcel    TC_002    Orangehrm
    LaunchTheApplication    ${URL}    ${BROWSERNAME}
    User Name And Password    ${testdata}[Username]    ${testdata}[Password]
    OpenPIM
    ConfigurationPIM
    DataImport.PIM
    ChooseFile.DataImport.PIM    ${EXECDIR}\\ImportFiles\\Nikitha_Resume_CSQA.doc

TC425_Verify user is not able to import file greater than 1MB
