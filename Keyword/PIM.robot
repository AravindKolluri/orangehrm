*** Settings ***
Resource          ../Global/Super.robot

*** Keywords ***
OpenPIM
    Wait Until Element Is Visible    ${PIM.Path}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${PIM.Path}

ConfigurationPIM
    Wait Until Element Is Visible    ${Configuration.PIM}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${Configuration.PIM}

OptionalFieldsPIM
    Wait Until Element Is Visible    ${OptionalFields.PIM}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${OptionalFields.PIM}

EditOptionalPIM
    Wait Until Element Is Visible    ${EditButton.PIM}    ${MEDIUMWAITS}
    FOR    ${key}    IN RANGE    1    4
        Element Should Be Visible    (//li[@class='checkbox']/input[@disabled='disabled'])[${key}]
    END
    SeleniumLibrary.Click Element    ${EditButton.PIM}
    Wait Until Element Is Visible    ${OptionalsCheckbox.PIM}    ${MEDIUMWAITS}
    FOR    ${key}    IN RANGE    1    4
        Click Element    (//input[@type='checkbox'])[${key}]
    END

SaveOptionalPIM
    Wait Until Element Is Visible    ${SaveButton.PIM}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${SaveButton.PIM}
    Wait Until Element Is Visible    ${EditButton.PIM}    ${MEDIUMWAITS}

CustomFieldsPIM
    Wait Until Element Is Visible    ${CustomFields.PIM}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${CustomFields.PIM}

AddCustomPIM
    Wait Until Element Is Visible    ${CustomAddButton.PIM}    ${MEDIUMWAITS}
    Click Element    ${CustomAddButton.PIM}
    Element Should Be Visible    ${CustomTextbox.PIM}

AddDataCustomPIM
    Input Text    ${CustomTextbox.PIM}    Nikitha
    Click Element    ${CustomsCheckbox.Screen.PIM}
    Click Element    ${Customs.Personal.PIM}
    Click Element    ${CustomsCheckbox.type.PIM}
    Click Element    ${Customstype.text.PIM}
    Click Element    ${OptionsSaveButton.PIM}

AddOnlyTextCustomPIM
    Input Text    ${CustomTextbox.PIM}    Nikitha
    Click Element    ${OptionsSaveButton.PIM}
    Element Should Be Visible    (//span[text()='Required'])[1]

AddOnlyScreenCustomPIM
    Click Element    ${CustomsCheckbox.Screen.PIM}
    Click Element    ${Customs.Personal.PIM}
    Click Element    ${OptionsSaveButton.PIM}
    Element Should Be Visible    (//span[text()='Required'])[1]

AddOnlyTypeCustomPIM
    Click Element    ${CustomsCheckbox.type.PIM}
    Click Element    ${Customstype.text.PIM}
    Click Element    ${OptionsSaveButton.PIM}
    Element Should Be Visible    (//span[text()='Required'])[1]

DataImport.PIM
    Wait Until Element Is Visible    ${DataImport.PIM}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${DataImport.PIM}

ChooseFile.DataImport.PIM
    [Arguments]    ${file}
    Wait Until Element Is Visible    ${ChoosefileImport.PIM}    ${MEDIUMWAITS}
    SeleniumLibrary.Double Click Element    ${ChoosefileImport.PIM}
    Add.Choosefile    ${file}
    Add.Choosefile    ${file}
