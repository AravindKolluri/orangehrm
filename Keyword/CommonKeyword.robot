*** Settings ***
Resource          ../Global/Super.robot

*** Keywords ***
LaunchTheApplication
    [Arguments]    ${url}    ${browser_name}
    Run Keyword If    '${browser_name}'=='Chrome' or '${browser_name}'=='chrome' or '${browser_name}'=='gc'    SeleniumLibrary.Open Browser    ${url}    Chrome
    Run Keyword If    '${browser_name}'=='Firefox' or '${browser_name}'=='firefox' or '${browser_name}'=='ff'    SeleniumLibrary.Open Browser    ${url}    Firefox
    Maximize Browser Window

ReadTestdataFromExcel
    [Arguments]    ${testcasesid}    ${sheetname}
    ${testdata}    CustomLibrary.Get Ms Excel Row Values Into Dictionary Based On Key    ${TESTDATA_FOLDER}\\OrangeHRM.xls    ${testcasesid}    ${sheetname}
    Set Global Variable    ${testdata}

User Name And Password
    [Arguments]    ${username}    ${password}
    Wait Until Element Is Visible    ${Element.loginpage.loginpanel}    ${LONGWAITS}    Loginpage is not visible after waitying for ${LONGWAITS}
    SeleniumLibrary.Input Text    ${Textfiled.loginpage.username}    ${username}
    SeleniumLibrary.Input Text    ${Textfiled.loginpage.password}    ${password}
    SeleniumLibrary.Click Element    ${Element.loginpage.login}

UploadFile
    [Arguments]    ${file}
    sleep    3s
    Comment    AutoItLibrary.Wait For Active Window    Open
    AutoItLibrary.Process Wait    ${EMPTY}    1
    AutoItLibrary.Control Send    Open    ${EMPTY}    [CLASS:Edit]    ${file}
    AutoItLibrary.Process Wait    ${EMPTY}    2
    AutoItLibrary.Control Command    Open    ${EMPTY}    [CLASS:ComboBox; INSTANCE:2]    SelectString    All Files (.)
    AutoItLibrary.Process Wait    ${EMPTY}    2
    AutoItLibrary.Control Click    Open    ${EMPTY}    [TEXT:&Open]

Dynamic Variable
    [Arguments]    ${locator}    ${value}
    ${xpath}    ReplaceString    ${locator}    replaceText    ${value}
    [Return]    ${xpath}

NavigateMenu
    [Arguments]    ${menu}
    ${homepage.leave.new}    Dynamic Variable    ${homepage.leave}    ${menu}
    SeleniumLibrary.Wait Until Element Is Visible    ${homepage.leave.new}    ${LONGWAITS}    ${menu} Not visible after waiting ${LONGWAITS}
    SeleniumLibrary.Click Element    ${homepage.leave.new}

NavigateToLeaveSubModule
    [Arguments]    ${menu}
    ${homepage.entitlement.new}    Dynamic Variable    ${homepage.entitlement}    ${menu}
    SeleniumLibrary.Wait Until Element Is Visible    ${homepage.entitlement.new}    ${MEDIUMWAITS}    ${menu} Not visible after waiting ${LONGWAITS}
    SeleniumLibrary.Click Element    ${homepage.entitlement.new}
