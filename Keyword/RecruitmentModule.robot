*** Settings ***
Resource          ../Global/Super.robot

*** Variables ***
${text1}          ${EMPTY}
@{jobstitle}
${jobs}           ${EMPTY}
${sData}          ${EMPTY}
${text2}          ${EMPTY}

*** Keywords ***
OpenRecruitmentModule
    Comment    wait for Recruitment Module
    Wait Until Element Is Visible    ${homepage.Recruitment.navigate}    ${MEDIUMWAITS}    Recruitment page is not open after waiting ${MEDIUMWAITS}
    Comment    Click on Recruitment Module
    SeleniumLibrary.Click Element    ${homepage.Recruitment.navigate}

Recruitement
    Element Text Should Be    ${Element.Recruitment.Candidates}    Candidates
    ${text1}    Get Text    ${Element.Recruitment.Candidates}
    Run Keyword If    '${text1}'=='Candidates'    Log    Test Case pass Sucesfully

List Elements
    Click Element    ${Element.Recruitment.Candidates}
    Click Element    ${Element.Recruitment.candidate.jobtitle}
    Page Should Contain List    ${Element.Recruitment.candidate.jobtitle}
    Select From List By Index    ${Element.Recruitment.candidate.jobtitle}    1
    Sleep    2
    @{jobstitle}    Get List Items    ${Element.Recruitment.candidate.jobtitle}
    FOR    ${jobs}    IN    @{jobstitle}
        LOG    ${jobs}
    END
    Sleep    2

List Hiring manager
    Click Element    ${Element.Recruitment.Candidates}
    Click Element    ${Element.Recruitment.candidate.hiringmanager}
    Select From List By Index    ${Element.Recruitment.candidate.hiringmanager}    1
    Sleep    2
    @{jobstitle}    Get List Items    ${Element.Recruitment.candidate.hiringmanager}
    FOR    ${jobs}    IN    @{jobstitle}
        LOG    ${jobs}
    END
    Sleep    2

List Status
    Click Element    ${Element.Recruitment.Candidates}
    Click Element    ${Element.Recruitment.candidate.status}
    Select From List By Index    ${Element.Recruitment.candidate.status}    1
    Sleep    2
    @{jobstitle}    Get List Items    ${Element.Recruitment.candidate.status}
    FOR    ${jobs}    IN    @{jobstitle}
        LOG    ${jobs}
    END
    Sleep    2

Input Text Keyword
    [Arguments]    ${Keyword}
    Click Element    ${Element.Recruitment.Candidates}
    Input Text    ${Element.Recruitment.candidate.Keyword}    ${Keyword}
    ${text1}    Get Text    ${Element.Recruitment.candidate.Keyword}
    Run Keyword If    '${text1}'=='candidateSearch_keywords'    Log    ${text1}

Methods dropdown list
    Click Element    ${Element.Recruitment.Candidates}
    Click Element    ${Element.Recruitment.Candidate.MethodApplication}
    Select From List By Index    ${Element.Recruitment.Candidate.MethodApplication}    1
    Sleep    2
    @{jobstitle}    Get List Items    ${Element.Recruitment.Candidate.MethodApplication}
    FOR    ${jobs}    IN    @{jobstitle}
        LOG    ${jobs}
    END
    Sleep    2

Patricular Vacancy
    Click Element    ${Element.Recruitment.Candidates}
    Click Element    ${Element.Recruitment.Vacancy}
    Sleep    2
    Select From List By Index    ${Element.Recruitment.Vacancy}    2
    Sleep    2

Candidate name
    [Arguments]    ${CandidateName}
    Click Element    ${Element.Recruitment.Candidates}
    Input Text    ${Textfiled.Recruitment.Candidatename}    ${CandidateName}

List Vacancy
    Click Element    ${Element.Recruitment.Candidates}
    Click Element    ${Element.Recruitment.Vacancy}
    Select From List By Index    ${Element.Recruitment.Vacancy}    1
    Sleep    2
    @{jobstitle}    Get List Items    ${Element.Recruitment.Vacancy}
    FOR    ${jobs}    IN    @{jobstitle}
        LOG    ${jobs}
    END
    Sleep    2

From Date To Date
    [Arguments]    ${FromDate}    ${ToDate}
    Click Element    ${Element.Recruitment.Candidates}
    Input Text    ${Element.Recruitment.Candidate.Fromdate}    ${FromDate}
    Input Text    ${Element.Recruitment.candidate.Todate}    ${ToDate}

Click ADD button
    Click Element    ${Element.Recruitment.Candidates}
    Click Element    ${Recruitment.ADDbutton}

ADD Valid Data
    [Arguments]    ${FirstName}    ${MiddleName}    ${LastName}    ${MailID}    ${MobileNumber}
    Click Element    ${Element.Recruitment.Candidates}
    Click Element    ${Recruitment.ADDbutton}
    Input Text    ${Textfiled.Recruitment.Add.Firstname}    ${FirstName}
    Input Text    ${Textfield.Recruitment.Add.Middlename}    ${MiddleName}
    Input Text    ${Textfiled.Recruitment.Add.LastName}    ${LastName}
    Input Text    ${Textfiled.Recruitment.Add.email}    ${MailID}
    Input Text    ${Textfiled.Recruitment.Add.Contactnumber}    ${MobileNumber}
    SeleniumLibrary.Select From List By Index    ${Textfiled.Recruitment.Add.jobVacancy}    1
    SeleniumLibrary.Double Click Element    ${Recruitement.candidate.uploadfile}

Reset Data
    [Arguments]    ${CandidateName}    ${Keyword}
    Click Element    ${Element.Recruitment.Candidates}
    Click Element    ${Element.Recruitment.candidate.jobtitle}
    Sleep    2
    Select From List By Index    ${Element.Recruitment.candidate.jobtitle}    2
    Click Element    ${Element.Recruitment.Vacancy}
    Sleep    2
    Select From List By Index    ${Element.Recruitment.Vacancy}    0
    Click Element    ${Element.Recruitment.candidate.hiringmanager}
    Sleep    2
    Select From List By Index    ${Element.Recruitment.candidate.hiringmanager}    0
    Click Element    ${Element.Recruitment.candidate.status}
    Sleep    2
    Select From List By Index    ${Element.Recruitment.candidate.status}    2
    Input Text    ${Textfiled.Recruitment.Candidatename}    ${CandidateName}
    Input Text    ${Element.Recruitment.candidate.Keyword}    ${Keyword}
    From Date To Date    ${testdata}[FromDate]    ${testdata}[ToDate]
    Click Element    ${Element.Recruitment.Candidate.MethodApplication}
    Sleep    2
    Select From List By Index    ${Element.Recruitment.Candidate.MethodApplication}    1
    Click Element    ${Recruitment.Candidate.ResetButton}

Null Data
    Click Element    ${Element.Recruitment.Candidates}
    Sleep    2
    Click Element    ${Recruitment.ADDbutton}
    Sleep    2
    Click Element    ${Recruitment.ADD.Savebutton}
    Sleep    2
    Element Text Should Be    ${Recruitment.ADD.ErrorMassage}    Required
    Sleep    2
    ${text1}    Get Text    ${Recruitment.ADD.ErrorMassage}
    Run Keyword If    '${text1}'=='Required'    Log    ${text1}
    Log    Test case pass sucessfully

Add.Choosefile
    [Arguments]    ${file}
    Comment    Wait Until Element Is Visible    ${Recruitement.candidate.uploadfile}    ${MEDIUMWAITS}
    Comment    SeleniumLibrary.Click Element    ${Recruitement.candidate.uploadfile}
    UploadFile    ${file}
    UploadFile    ${file}

AddDataTextFields
    [Arguments]    ${AddKeyword}    ${AddComment}
    Input Text    ${Add.Keyword}    ${AddKeyword}
    Input text    ${Add.Comment}    ${AddComment}
    Click Element    ${Add.ChooseKeepdata}
    Click Button    ${Recruitment.ADD.Savebutton}

Add Invalid Data
    [Arguments]    ${FirstName}    ${MiddleName}    ${LastName}    ${MailID}    ${MobileNumber}
    Click Element    ${Element.Recruitment.Candidates}
    Click Element    ${Recruitment.ADDbutton}
    Input Text    ${Textfiled.Recruitment.Add.Firstname}    ${FirstName}
    Input Text    ${Textfield.Recruitment.Add.Middlename}    ${MiddleName}
    Input Text    ${Textfiled.Recruitment.Add.LastName}    ${LastName}
    Input Text    ${Textfiled.Recruitment.Add.email}    ${MailID}
    Input Text    ${Textfiled.Recruitment.Add.Contactnumber}    ${MobileNumber}
    SeleniumLibrary.Select From List By Index    ${Textfiled.Recruitment.Add.jobVacancy}    1
    SeleniumLibrary.Double Click Element    ${Recruitement.candidate.uploadfile}

ErrorMassageRequired
    Element Text Should Be    ${Add.Error.formatechange}    Expected format: admin@example.com
    Sleep    2
    ${text1}    Get Text    ${Add.Error.formatechange}
    Run Keyword If    '${text1}'=='Expected format: admin@example.com'    Log    ${text1}
    Log    Test case pass sucessfully

Edit Data
    SeleniumLibrary.Click Element    ${Edite.Button.Recruitment}
    Sleep    2

Cancel Data
    SeleniumLibrary.Click Element    ${Add.Cancel.data}
    Sleep    2

back to navigate
    Sleep    2
    SeleniumLibrary.Click Button    ${Add.back}
    Sleep    2

Search Vacancy
    SeleniumLibrary.Click Element    ${Recruitment.vancancy}
    SeleniumLibrary.Click Element    ${Vacancy.jobtitle}
    Select From List By Index    ${Vacancy.jobtitle}    1
    Sleep    2
    SeleniumLibrary.Click Element    ${Vacancy.vacancydrop}
    Select From List By Index    ${Vacancy.vacancydrop}    0
    Sleep    2
    SeleniumLibrary.Click Element    ${Vacancy.hiringmanager}
    Select From List By Index    ${Vacancy.hiringmanager}    0
    Sleep    2
    SeleniumLibrary.Click Element    ${Vacancy.search}
    Select From List By Index    ${Vacancy.search}    1
    Sleep    2
    Click Element    ${Vacancy.search.btn}
