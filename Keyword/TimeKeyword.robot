*** Settings ***
Resource          ../Global/Super.robot

*** Keywords ***
OpenTimeModule
    Comment    wait for Time Module
    SeleniumLibrary.Click Element    ${homepage.time}
    Comment    Click on Leave Module

ClickOnReportsModule
    Comment    wait for Reports Module
    Wait Until Element Is Visible    ${homepage.time.repots}    ${MEDIUMWAITS}    Reports label is not open after waiting ${MEDIUMWAITS}
    Comment    Click on Leave Module
    SeleniumLibrary.Click Element    ${homepage.time.repots}

ClickOnAttendanceSummary
    Comment    wait for ProjectsReports Module
    Wait Until Element Is Visible    ${time.repots.attendancesummary}    ${MEDIUMWAITS}    attendancesummary label is not open after waiting ${MEDIUMWAITS}
    Comment    Click on Leave Module
    SeleniumLibrary.Click Element    ${time.repots.attendancesummary}

ClickOnProjectInfoDropdown
    Comment    wait for ProjectsReports Module
    Wait Until Element Is Visible    ${time.projectinfo}    ${MEDIUMWAITS}    Projectinfo label is not open after waiting ${MEDIUMWAITS}
    Comment    Click on Leave Module
    SeleniumLibrary.Click Element    ${time.projectinfo}

ClickOnCustomers
    Comment    wait for ProjectsReports Module
    Wait Until Element Is Visible    ${time.projectinfo.customers}    ${MEDIUMWAITS}    Customers label is not open after waiting ${MEDIUMWAITS}
    Comment    Click on Leave Module
    SeleniumLibrary.Click Element    ${time.projectinfo.customers}

Add Customers
    [Arguments]    ${customername}
    Comment    wait for ProjectsReports Module
    Wait Until Element Is Visible    ${button.time.projectinfo.customers.add}    ${MEDIUMWAITS}    Customers button l is not open after waiting ${MEDIUMWAITS}
    Comment    Click on Time Module
    SeleniumLibrary.Click Element    ${button.time.projectinfo.customers.add}
    SeleniumLibrary.Input Text    ${textfield.time.projectifo.customer.add}    ${customername}
    SeleniumLibrary.Click Element    ${button.time.projectinfo.customer.add.save}
    Wait Until Element Is Visible    //*[text()='234@sr']

Add customer with customer name is blank
    Comment    wait for ProjectsReports Module
    Wait Until Element Is Visible    ${button.time.projectinfo.customers.add}
    Comment    Click on Time Module
    SeleniumLibrary.Click Element    ${button.time.projectinfo.customers.add}
    SeleniumLibrary.Click Element    ${button.time.projectinfo.customer.add.save}
    Wait Until Element Is Visible    //*[@id="frmAddCustomer"]/fieldset/ol/li[1]/span

Delete Customer
    Comment    SeleniumLibrary.Click Element    ${checkbox.time.projectinfo.customer.delete}
    SeleniumLibrary.Click Element    //*[@type="checkbox" and @value="13"]
    SeleniumLibrary.Click Element    ${button.time.projectinfo.customer.delete}
    SeleniumLibrary.Click Element    ${button.time.customer.delete.confirmation}

Add Project
    SeleniumLibrary.Click Element    ${dropdown.time.projectinfo.project}
    SeleniumLibrary.Click Element    ${button.time.projectinfo.projects.add}
    SeleniumLibrary.Click Element    ${label.time.projectinfo.projects.add.prjects.customername}
    Input Text    ${textfield.time.projectinfo.projects.addproject.customername.}    hrm
    Select From List By Value    /html/body/div[4]/ul/li
    SeleniumLibrary.Click Element    ${button.time.projectinfo.projects.customername.save}
    Comment    ${LONGWAITS}
    Input Text    ${textfeild.time.projectinfo.projects.add.projectname}    228
    SeleniumLibrary.Click Element    ${button.time.projectinfo.projects.add.project.save}

Searchfor theproject
    SeleniumLibrary.Click Element    ${dropdown.time.projectinfo.project}
    Input Text    ${textfeid.time.projectinfo.projects.projectname}    ACME Ltd
    Comment    Press Keys
    Input Text    ${textfeild.time.projectinfo.projects.searchprojects.customer name}    ACME Ltd
    SeleniumLibrary.Click Element    ${button.time.projectinfo.projects.project.search}
    Wait Until Element Is Visible    //*[@id="resultTable"]/tbody/tr/td[3]/a

Search project with blank details
    SeleniumLibrary.Click Element    ${dropdown.time.projectinfo.project}
    SeleniumLibrary.Click Element    ${button.time.projectinfo.projects.project.search}

Invalid Project name and try to add the project
    SeleniumLibrary.Click Element    ${dropdown.time.projectinfo.project}
    SeleniumLibrary.Click Element    ${button.time.projectinfo.projects.add}
    SeleniumLibrary.Click Element    ${textfield.time.projectinfo.projects.addproject.customername.}
    Input Text    ${textfield.time.projectinfo.projects.addproject.customername.}    Apache Software Foundation
    Select From List By Index    // *[@class="ac_even" and text()='oftware Foundation']
    Comment    Comment    Comment    Comment    Comment    Comment    Comment    Comment    Comment    Comment    Comment    Comment    Select From List By Value    ACME Ltd

Open PIM Module
    SeleniumLibrary.Click Element    ${label.pim}

Open Employee List
    SeleniumLibrary.Click Element    ${dropdown.pim.employelist}
    Wait Until Element Is Visible    ${label.pim.employelist}

Search Employee List
    Input Text    ${textfeild.pim.employeelist.employeeid}    0272
    SeleniumLibrary.Click Element    ${button.pim.employeelist.search}
    Wait Until Element Is Visible    //*[@id="resultTable"]/tbody/tr/td[2]/a

Add employee
    SeleniumLibrary.Click Element    ${button.pim.employeelist.addemployee}
    Wait Until Element Is Visible    //*[@id="content"]/div/div[1]/h1

Acess Reports
    SeleniumLibrary.Click Element    ${label.pim.reports}
    Wait Until Element Is Visible    //*[@id="content"]/div[1]/a
