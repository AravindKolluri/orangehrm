*** Settings ***
Resource          ../Global/Super.robot

*** Keywords ***
Navigate to configure module
    [Arguments]    ${configure.tab}
    Wait Until Element Is Visible    ${configure.tab}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${configure.tab}

Click holiday dropdown
    [Arguments]    ${Holidays.tab}
    Wait Until Element Is Visible    ${Holidays.tab}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${Holidays.tab}

click on the holiday check box
    [Arguments]    ${checkbox.tab}
    Wait Until Element Is Visible    ${checkbox.tab}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${checkbox.tab}

click on delete button
    [Arguments]    ${button.delete}
    Wait Until Element Is Visible    ${button.delete}    ${LONGWAITS}
    SeleniumLibrary.Click Element    ${button.delete}

check box one Leavelist
    Wait Until Element Is Visible    ${Elements.leavelists}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${Elements.leavelists}

open leavelists
    Wait Until Element Is Visible    ${Elements.leavelists}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${Elements.leavelists}

checkboxAll
    [Arguments]    ${Elements.checkboxAll.tab}
    Wait Until Element Is Visible    ${Elements.checkboxAll}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${Elements.checkboxAll}
    comment    ${checkbox.tab}
    comment    Wait Until Element Is Visible    ${checkbox.tab}    ${MEDIUMWAITS}

calender
    Wait Until Element Is Visible    ${Textfield.Fromdate}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${Textfield.Fromdate}
    Wait Until Element Is Visible    ${Element.datePicker.dropdown}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${Element.datePicker.dropdown}
    Wait Until Element Is Visible    ${Elements.selectyear}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${Elements.selectyear}
    Wait Until Element Is Visible    ${Element.fromdate}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${Element.fromdate}
    Wait Until Element Is Visible    ${Textfield.Todate}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${Textfield.Todate}
    Wait Until Element Is Visible    ${Element.Todate}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${Element.Todate}

select actions
    Comment    Wait Until Element Is Visible    ${Elements.selectyear}    ${MEDIUMWAITS}
    Comment    SeleniumLibrary.Click Element    ${Elements.selectyear}
    Wait Until Element Is Visible    ${Elements.select actions.Approve}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${Elements.select actions.Approve}

click on back
    [Arguments]    ${button.back}
    Wait Until Element Is Visible    ${button.back}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Button    ${button.back}

select date
    Wait Until Element Is Visible    ${Elements.EmployeeLeave.Date}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${Elements.EmployeeLeave.Date}

click on Reset
    Wait Until Element Is Visible    ${button.Reset}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Button    ${button.Reset}

click assign leave module
    Wait Until Element Is Visible    ${Elements.AssignLeaveModule}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Button    ${Elements.AssignLeaveModule}

Employee
    [Arguments]    ${Employee}
    Wait Until Element Is Visible    ${Textfield.Employee}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${Textfield.Employee}
    SeleniumLibrary.Input Text    ${Textfield.Employee}    ${Employee}

OpenAddEntitlement
    Comment    wait for AddEntitlement Element
    SeleniumLibrary.Wait Until Element Is Visible    ${homepage.entitilement.addentitlement}
    SeleniumLibrary.Click Element    ${homepage.entitilement.addentitlement}

Enter Details in Add Leave Entitlement
    [Arguments]    ${name}    ${entitlement}
    Comment    SeleniumLibrary.Wait Until Element Is Visible    ${Textfiled.leave.addleave}    {LONGWAITS}
    SeleniumLibrary.Clear Element Text    ${Textfiled.leave.addleave}
    sleep    15
    SeleniumLibrary.Input Text    ${Textfiled.leave.addleave}    ${name}
    sleep    3
    comment    select in the below drop down which is frist name
    Press Keys    None    ENTER
    SeleniumLibrary.Input Text    ${Textfiled.add entitlement.entitlement}    ${entitlement}
    SeleniumLibrary.Click Button    ${button.leave.addleave.save}

AddLeave Cancel button
    SeleniumLibrary.Click Button    ${button.leave.addleave.cancel}
    SeleniumLibrary.Wait Until Element Is Visible    ${Elemenet.leave.leave entitlement}    ${LONGWAITS}    LeaveEntitlement elememnt is not visible after waiting ${LONGWAITS}

Updating Entitlement
    comment    wait for confirm button
    SeleniumLibrary.Wait Until Element Is Visible    ${leave.addleave.updatingentitlement.confrim}    ${LONGWAITS}    confrim button is not visible after waiting ${LONGWAITS}
    SeleniumLibrary.Click Button    ${leave.addleave.updatingentitlement.confrim}

Enter Details in leave Entitlement
    [Arguments]    ${name}
    SeleniumLibrary.Input Text    ${TextFiled.leave.leave entitlement.employee}    ${name}
    Press Keys    None    ENTER
    SeleniumLibrary.Click Button    ${button.leave.leave Entitlement.search}

openMyEntitlement
    [Arguments]    ${homepage.entitlement.myentitlement}
    Wait Until Element Is Visible    ${homepage.entitlement.myentitlement}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${homepage.entitlement.myentitlement}

clickonsearch
    [Arguments]    ${button.leave.leave Entitlement.search}
    Wait Until Element Is Visible    ${button.leave.leave Entitlement.search}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Button    ${button.leave.leave Entitlement.search}

clickonadd
    Wait Until Element Is Visible    ${button.leave.leave Entitlement.search.add}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Button    ${button.leave.leave Entitlement.search.add}

clickonsave
    Wait Until Element Is Visible    ${button.leave.leave Entitlement.search.add.save}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Button    ${button.leave.leave Entitlement.search.add.save}

addtomultiple
    Wait Until Element Is Visible    ${button.leave.leave Entitlement.search.add.addmultiple}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Button    ${button.leave.leave Entitlement.search.add.addmultiple}

clickonapply
    Wait Until Element Is Visible    ${homepage.leave.apply}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${homepage.leave.apply}

Navigateapply
    SeleniumLibrary.Wait Until Element Is Visible    ${homepage.leave.navigateapply}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${homepage.leave.navigateapply}

clickonleave
    SeleniumLibrary.Wait Until Element Is Visible    ${button.leave}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${button.leave}

clickonconfigure
    SeleniumLibrary.Wait Until Element Is Visible    ${homepage.leave.configure}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${homepage.leave.configure}

clickonleaveperiod
    Wait Until Element Is Visible    ${homepage.leave.configure.leaveperiod}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${homepage.leave.configure.leaveperiod}

leavetype
    [Arguments]    ${Entitlement}
    SeleniumLibrary.Wait Until Element Is Visible    ${TextFiled.leave.leave entitlement.myentitlement.leavetype.dropdown}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${TextFiled.leave.leave entitlement.myentitlement.leavetype.dropdown}
    SeleniumLibrary.Wait Until Element Is Visible    ${TextFiled.leave.leave entitlement.myentitlement.leavetype.entitlement}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${TextFiled.leave.leave entitlement.myentitlement.leavetype.entitlement}    ${Entitlement}

NavigateToMyentitlements
    [Arguments]    ${eliments.entitlements.myentitlementsdropdown}
    Wait Until Element Is Visible    ${eliments.entitlements.myentitlementsdropdown}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${eliments.entitlements.myentitlementsdropdown}

Navigate to Configure
    SeleniumLibrary.Click Element    ${list.Configure}
    SeleniumLibrary.Mouse Over    //a[@id="menu_leave_leaveTypeList"]
    SeleniumLibrary.Click Element    //a[@id="menu_leave_leaveTypeList"]

Enter Add Button
    SeleniumLibrary.Click Element    btnAdd

Enter valid details in Name field
    [Arguments]    ${Name}
    SeleniumLibrary.Input Text    ${textfield.Name}    ${Name}

Enter Cancel Button
    SeleniumLibrary.Click Element    //input[@name="backButton"]

Enter Edit Button
    SeleniumLibrary.Click Element    //input[@value="Edit"]

Enter From and To dates of Future year and Search
    [Arguments]    ${fromdate}    ${todate}
    SeleniumLibrary.Input Text    ${textfield.holidays.fromdate}    ${fromdate}
    SeleniumLibrary.Input Text    ${textfield.holidays.todate}    ${todate}
    SeleniumLibrary.Click ELement    //input[@name="btnSearch"]

Enter Hide Button
    SeleniumLibrary.Click Element    //a[@class="toggle tiptip"]

Enter Add Holiday Button
    SeleniumLibrary.Click Element    //input[@id="btnAdd"]

Enter Leave Period Edit Button
    SeleniumLibrary.Click Element    //input[@name="btnEdit"]

Enter Ok Button
    SeleniumLibrary.Click Element    //input[@id="dialogDeleteBtn"]

Adding holiday
    Wait Until Element Is Visible    ${button.add.holiday}    ${MEDIUMWAITS}
    SeleniumLibrary.Click Element    ${button.add.holiday}

Popup Update entitlement
    ${status}    Run Keyword And Return Status    SeleniumLibrary.Wait Until Element Is Visible    ${button.leave.entitlement.update}    ${LONGWAITS}    update button is not visible after waiting ${LONGWAITS}
    Run Keyword If    ${status}==True    SeleniumLibrary.Click Button    ${button.leave.entitlement.update}
