*** Keywords ***
Open Configure
    Open Performance Module
    Wait Until Element Is Visible    ${Performance.Configure}    ${MEDIUMWAITS}
    Comment    Once element is visible,Click on configure
    SeleniumLibrary.Click Element    ${Performance.Configure}

Open Performance Module
    Comment    Opening Performance module
    Wait Until Element Is Visible    ${homepage.performance}    ${MEDIUMWAITS}
    Comment    Once element is visible,Click on performance Module
    SeleniumLibrary.Click Element    ${homepage.performance}

Open KPI
    Comment    Click KPI from dropdwon in Configure section
    Wait Until Element Is Visible    ${Configure.KPI}    ${MEDIUMWAITS}
    Comment    Once element is visible,Click on KPI from dropdown
    SeleniumLibrary.Click Element    ${Configure.KPI}

Open Tracker
    Comment    Click Tracker from dropdwon in Configure section
    Wait Until Element Is Visible    ${Configure.trackers}    ${MEDIUMWAITS}
    Comment    Once element is visible,Click on tracker from dropdown
    Click Element    ${Configure.trackers}

Open Manage Reviews
    Open Performance Module
    Wait Until Element Is Visible    ${Performance.manage_reviews}    ${MEDIUMWAITS}
    Comment    Once element is visible,Click on manage reviews
    SeleniumLibrary.Click Element    ${Performance.manage_reviews}

Add Details in KPI
    [Arguments]    ${KPI}    ${Minrating}    ${Maxrating}
    Comment    Add details in the textfields and save.
    SeleniumLibrary.Click Element    ${KPI.Adddetails}
    SeleniumLibrary. InputText    ${KPI.textfield}    ${KPI}
    SeleniumLibrary. InputText    ${Minrating.textfield}    ${Minrating}
    SeleniumLibrary. InputText    ${Maxrating.textfield}    ${Maxrating}
    Press Keys    None    BACKSPACE
    Comment    SeleniumLibrary.Click Element    ${Add.checkbox}

Add details in Tracker
    [Arguments]    ${Trackername}    ${Employeename}
    Comment    Add details in the textfields and save.
    SeleniumLibrary. InputText    ${Trackername.textfield}    ${Trackername}
    SeleniumLibrary. InputText    ${Employeename.textfield}    ${Employeename}
    SeleniumLibrary.Click Element    ${Tracker.add_details}
    SeleniumLibrary.Click Element    ${trackers.scrolldown_dp}
    SeleniumLibrary.Click Element    ${Tracker.Addselected_dp}
    SeleniumLibrary.Click Element    ${Trackers.add_remove}
    Comment    selecting the removed date again to save the details
    SeleniumLibrary.Click Element    ${trackers.scrolldown_dp}
    SeleniumLibrary.Click Element    ${Tracker.Addselected_dp}
