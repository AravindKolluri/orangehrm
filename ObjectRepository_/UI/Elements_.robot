*** Variables ***
${Element.loginpage.loginpanel}    //div[text()='LOGIN Panel']
${Element.loginpage.login}    //input[@id='btnLogin']
${Elements.loginpage.invalid}    //span[text()='Invalid credentials']
${Elements.loginpage.cann't be empty }    //span[text()='Username cannot be empty']
${homepage.common.module}    (//ul[@id='mainMenuFirstLevelUnorderedList']//li//a//b)[3]
${leavepage.tab}    //b[text()='Leave']
${configure.tab}    //a[@id='menu_leave_Configure']
${Holidays.tab}    //a[@id='menu_leave_viewHolidayList']    # //a[text()='Holidays']
${list.logout}    //a[text()="Logout"]
${checkbox.tab}    //input[@id='ohrmList_chkSelectRecord_11']
${Elements.checkboxAll.tab}    //input[@id='leaveList_chkSearchFilter_checkboxgroup_allcheck']    # //input[@id='ohrmList_chkSelectAll']
${Elements.leavelists}    //a[@id='menu_leave_viewLeaveList']
${Element.action}    //select[@id='select_leave_action_67']
${Elements.dropdown}    //select[@class='select_action quotaSelect']
${Element.fromdate}    //a[text()='4']
${Element.Todate}    //a[text()='9']
${Element.datePicker.dropdown}    //select[@class='ui-datepicker-year']
${Elements.selectyear}    //option[@value='2019']
${Elements.select actions}    //select[@id='select_leave_action_23']
${Elements.select actions.Approve}    (//option[text()='Cancel'])[1]    # (//option[text()='Approve'])[1]
${Elements.EmployeeLeave.Date}    //a[text()='2020-12-21 to 2020-12-23']
${Elements.AssignLeaveModule}    //a[text()='Assign Leave']
${homepage.time}    //*[@id="menu_time_viewTimeModule"]/b
${time.repots.attendancesummary}    //*[@id="menu_time_displayAttendanceSummaryReportCriteria"]
${homepage.Recruitment}    (//ul[@id='mainMenuFirstLevelUnorderedList']//li//a//b)[5]
${Element.Recruitment.Candidates}    //*[@id="menu_recruitment_viewCandidates"]
${Element.Recruitment.candidate.jobtitle}    //*[@id="candidateSearch_jobTitle"]
${Element.Recruitment.candidate.hiringmanager}    //*[@id="candidateSearch_hiringManager"]
${Element.Recruitment.candidate.status}    //*[@id="candidateSearch_status"]
${Element.Recruitment.candidate.Keyword}    //*[@id="candidateSearch_keywords"]
${Element.Recruitment.Candidate.MethodApplication}    //*[@id="candidateSearch_modeOfApplication"]
${Element.Recruitment.Vacancy}    //*[@id="candidateSearch_jobVacancy"]
${Recruitment.ADD.ErrorMassage}    //*[@id="frmAddCandidate"]/fieldset/ol[1]/li[1]/ol/li[1]/span
${homepage.Recruitment.navigate}    //b[text()='Recruitment']
${Element.Recruitment.Candidate.Fromdate}    //*[@id="candidateSearch_fromDate"]
${Element.Recruitment.candidate.Todate}    //*[@id="candidateSearch_toDate"]
${homepage.performance}    //a[@id='menu__Performance']
${Performance.Configure}    //a[@id='menu_performance_Configure']
${Configure.KPI}    //a[@id="menu_performance_searchKpi"]
${KPI.Jobtitle}    //select[@name='kpi360SearchForm[jobTitleCode]']
${Jobtitle.dropdown}    //select[@name='kpi360SearchForm[jobTitleCode]']/option[10]
${Configure.trackers}    //a[@id='menu_performance_addPerformanceTracker']
${Performance.manage_reviews}    //a[@id='menu_performance_ManageReviews']
${KPI.Adddetails}    //select[@name='defineKpi360[jobTitleCode]']/option[9]
${Add.checkbox}    //input[@name='defineKpi360[makeDefault]']
${Delete.checkbox}    //input[@id='ohrmList_chkSelectRecord_29']
${Tracker.add_details}    /html/body/div[4]/ul/li/strong
${trackers.scrolldown_dp}    //select/option[5]
${Trackers.remove_selected}    //select[@id='addPerformanceTracker_assignedEmp']/option
${Trackername.textfield}    //input[@id='addPerformanceTracker_tracker_name']
${Employeename.textfield}    //input[@name='addPerformanceTracker[employeeName][empName]']
${Tracker.deleteCB}    //input[@id='ohrmList_chkSelectRecord_5']
${Tracker.delete}    //input[@id='btnDelete']
${homepage.common.module}    (//ul[@id='mainMenuFirstLevelUnorderedList']//li//a//b)[3]
${PIM.Path}       //a[@id='menu_pim_viewPimModule']
${Configuration.PIM}    //a[@id='menu_pim_Configuration']
${OptionalFields.PIM}    //li/a[text()='Optional Fields']
${Disabledcb.Optional.PIM}    //li[@class='checkbox']/input[@disabled='disabled']
${CustomFields.PIM}    //li/a[text()='Custom Fields']
${OptionalsCheckbox.PIM}    //input[@type='checkbox']
${CustomsCheckbox.Screen.PIM}    //select[@id='customField_screen']
${Customs.Personal.PIM}    //option[@value='personal']
${CustomsCheckbox.type.PIM}    //select[@id='customField_type']
${Customstype.text.PIM}    //option[@value='0']
${DataImport.PIM}    //li/a[text()='Data Import']
${homepage.leave}    //b[text()='replaceText']
${homepage.common.allmodule}    (//ul[@id='mainMenuFirstLevelUnorderedList']//li//a//b)[3]
${homepage.entitlement}    //a[text()='replaceText']
${homepage.entitilement.addentitlement}    //a[text()='Add Entitlements']
${Elemenet.leave.leave entitlement}    //a[@class='toggle tiptip']
${homepage.entitlement.myentitlement}    //*[@id="menu_leave_viewMyLeaveEntitlements"]
${homepage.entitlement.myentitlement.add.selectlocation }    //select[@id='entitlements_filters_location']
${homepage.leave.apply}    //a[@id='menu_leave_applyLeave']
${homepage.leave.navigateapply}    //a[@id='menu_leave_applyLeave']
${homepage.leave.configure}    //a[@id='menu_leave_Configure']
${homepage.leave.configure.leaveperiod}    //a[@id='menu_leave_defineLeavePeriod']
${Recruitement.candidate.uploadfile}    //*[@id="addCandidate_resume"]
${eliments.entitlements.myentitlementsdropdown}    //a[@id='menu_leave_viewMyLeaveEntitlements']
${list.Configure}    //a[@id="menu_leave_Configure"]
${menutab.hide}    //a[@class="toggle tiptip"]
${checkbox.CANPersonal}    //input[@id="ohrmList_chkSelectRecord_7"]
${checkbox.leavetype}    //input[@id="ohrmList_chkSelectRecord_3"]
${checkbox.FMLA}    //input[@id="ohrmList_chkSelectRecord_5"]
${checkbox.Maternity}    //input[@id="ohrmList_chkSelectRecord_6"]
${Add.Error.formatechange}    //*[@id="frmAddCandidate"]/fieldset/ol[1]/li[2]/span
${Elements.checkboxAll}    //input[contains(@id,'_allcheck')]    # //input[@id='ohrmList_chkSelectAll']
${Vacancy.jobtitle}    //*[@id="vacancySearch_jobTitle"]
${Vacancy.vacancydrop}    //*[@id="vacancySearch_jobVacancy"]
${Vacancy.hiringmanager}    //*[@id="vacancySearch_hiringManager"]
${Vacancy.search}    //*[@id="vacancySearch_status"]
${Recruitment.vancancy}    //*[@id="menu_recruitment_viewJobVacancy"]
