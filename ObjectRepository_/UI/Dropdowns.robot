*** Variables ***
${time.projectinfo}    //*[@id="menu_admin_ProjectInfo"]
${time.projectinfo.customers}    //*[@id="menu_admin_viewCustomers"]
${dropdown.time.projectinfo.project}    //*[@id="menu_admin_viewProjects"]
${dropdown.time.projectinfo.projects.addprojects.customername}    /html/body/div[4]/ul/li[1]
${dropdown.pim.configuration}    //*[@id="menu_pim_Configuration"]
${dropdown.pim.configuration.teminationreason}    //*[@id="menu_pim_viewTerminationReasons"]
${dropdown.pim.employelist}    //*[@id="menu_pim_viewEmployeeList"]
${dropdown.pim.employeelist.status}    //*[@id="empsearch_employee_status"]
